# Street Rat - Phaser JS RPG project

A Phaser 3 project for practice and fun. Uses [Babel 7](https://babeljs.io) and [Webpack 4](https://webpack.js.org/)


## Live Demo
[Demo Site](http://street-rat.duckdns.org:8080)
